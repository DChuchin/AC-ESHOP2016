'use strict';

/**
 * Generate build version
 * @return {String} build version
 */
module.exports = function setBuildVersion() {
    // build version is current date without spaces (replaced to _) and without time zone info.
    var date = new Date(),
        year = date.getFullYear().toString(),
        month = (date.getMonth() + 1).toString(),
        day = date.getDate().toString();

    return year + '-' + month + '-' + (day.length == 1 ? '0' + day : day);
};
