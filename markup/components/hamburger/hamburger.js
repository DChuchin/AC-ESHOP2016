
/*************************************************
* mobile menu
*/

var mobileMenu  = (function() {
  function init() {
    _setUpListeners();
  };
  function _setUpListeners() {
    $('.hamburger').on('click', _toggleMenu);

  };
  function _toggleMenu(e) {
    e.preventDefault();
    var $this = $(this),
        menu = $('.header__nav'),
        duration = 300;
    if ($this.hasClass('isOpened')) {
      $(menu).slideUp(duration, function() {
        $this.removeClass('isOpened');
      });
    } else {
      $this.addClass('isOpened');
      $(menu).slideDown(duration);
    };
  };

  function _openMenu() {

  }

  function _closeMenu() {

  }
  return {
    init: init
  };
})();

mobileMenu.init();
