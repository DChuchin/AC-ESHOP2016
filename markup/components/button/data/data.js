var data = {
  btnBigRed: {
    mod: "big-red",
    type: "button"
  },
  btnSmall: {
    mod: "small",
    type: "button",
    text: "invia"
  },
  btnGray: {
    mod: "gray",
    type: "button",
    text: 'Vedi Prodotto'
  },
  btnCart: {
    mod: "cart",
    type: "button",
    count: 0,
    text: 'carello'
  },
  btnCartEmpty: {
    mod: "cart empty",
    type: "button",
    count: 3,
    text: 'carello'
  },
  addCart: {
    mod: "add-cart",
    type: "button",
    text: 'Aggiungi'
  },
  submit: {
    mod: 'submit',
    type: 'submit',
    text: 'invia'
  },
  addCartSidebar: {
    mod: 'add-cart button--sidebar',
    type: 'button',
    text: 'Aggiungi al carrello'
  },
  continueShopping: {
    mod: 'continue button--add-cart',
    type: 'button',
    text: "procedi con l'acquisto"
  },
  back: {
    mod: 'back button--gray',
    type: 'button',
    text: 'ritorna al prodotto'
  },
  promo: {
    mod: 'promo',
    type: 'button',
    text: 'Applica'
  },
  btnContinue: {
    mod: 'next',
    type: 'button',
    text: 'Procedi'
  },
  submitCheckout: {
    mod: 'submit-checkout',
    type: 'submit',
    text: "Conferma l'ordine"
  },
  submitUserData: {
    mod: 'big-red',
    type: 'submit',
    text: 'Modifica'
  },
  submitBigRed: {
    mod: 'big-red',
    type: 'submit'
  },
  bigRed: {
    mod: 'big-red',
    type: 'button'
  },
  thanksButton: {
    mod: 'thanks',
    type: 'button',
    text: 'Torna al catalogo'
  },
  btnSure: {
    red: {
      mod: 'sure-red',
      type: 'button',
      text: 'Resta qui'
    },
    gray: {
      mod: 'sure-gray',
      type: 'button',
      text: 'Ritorna al carrello'
    }
  },
  btnLogout: {
    mod: 'logout',
    type: 'button'
  }
};
