/*************************************************
* Credits popup
*/

var creditsModule = (function() {
  function init() {
    _setUpListeners();
  };

  function _setUpListeners() {
    $('.copyright__link--credits').on('click', openCredits);
    $('.credits__bg').on('click', closeCredits);
  };

  function openCredits(e) {
    e.preventDefault();
    var credits = $('.credits__bg');
    credits.fadeIn(200);
    $('body').addClass('fixed');
  };

  function closeCredits(e) {
    e.preventDefault();
    var target = e.target;
    if ($(target).hasClass('credits__close-btn')||$(target).hasClass('credits__bg')) {
      $(this).fadeOut(200);
      $('body').removeClass('fixed');
    }
  };

  return {
    init: init
  };
})();

creditsModule.init();
