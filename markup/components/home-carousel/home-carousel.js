/*************************************************
* Home page slider
*/

$('.home-carousel').slick({
  dots: true,
  infinite: true,
  nextArrow: '<button type="button" class="home-carousel__btn home-carousel__btn--next">Next</button>',
  prevArrow: '<button type="button" class="home-carousel__btn home-carousel__btn--prev">Prev</button>',
  responsive: [
    {
      breakpoint: 640,
      settings: {
        dots:false
      }
    }
  ]
});
