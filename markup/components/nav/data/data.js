var data = {
  nav: [
    {
      title: 'Corporate line',
      link: '#',
      submenu: [
        {
          title: 'Manuali',
          link:'#'
        },
        {
          title: 'Infotech',
          link:'#'
        },
        {
          title: 'Corsi',
          link:'#'
        }
      ]
    },
    {
      title: 'Fashion line',
      link: '#',
      submenu: [
        {
          title: 'Manuali',
          link:'#'
        },
        {
          title: 'Infotech',
          link:'#'
        },
        {
          title: 'Corsi',
          link:'#'
        }
      ]
    },
    {
      title: 'Kids line',
      link: '#',
      submenu: [
        {
          title: 'Manuali',
          link:'#'
        },
        {
          title: 'Infotech',
          link:'#'
        },
        {
          title: 'Corsi',
          link:'#'
        }
      ]
    },
    {
      title: 'Da non perdere',
      link: '#',
      submenu: [
        {
          title: 'Manuali',
          link:'#'
        },
        {
          title: 'Infotech',
          link:'#'
        },
        {
          title: 'Corsi',
          link:'#'
        }
      ]
    },
    {
      title: 'Service',
      link: '#',
      submenu: [
        {
          title: 'Manuali',
          link:'#'
        },
        {
          title: 'Infotech',
          link:'#'
        },
        {
          title: 'Corsi',
          link:'#'
        }
      ]
    }
  ]
};