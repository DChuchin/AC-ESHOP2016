
/*************************************************
* Main navigation
*/

var navAccordeon = (function() {
  function init() {
    _setUpListeners();
  };
  function _setUpListeners() {
    $('.nav__item--parent').on('click', _toggleAccordeon);
    $(window).on('resize', _clearNav);
  };
  function _toggleAccordeon(e) {
    if (!$(e.target).hasClass('nav__link')) {
      e.preventDefault();
      var $this = $(this),
          innerList = $this.find('.nav__list--inner'),
          siblings = $this.siblings('.nav__item'),
          siblingsLists = siblings.find('.nav__list'),
          duration = 300;

      if ($this.hasClass('isOpened')) {
        $this.removeClass('isOpened')
        innerList.stop(true,true).slideUp(duration);
      } else {
        $this.addClass('isOpened');
        innerList.stop(true,true).slideDown(duration);
        siblingsLists.stop(true,true).slideUp(duration);
        siblings.removeClass('isOpened');
      }
    } else {
      $('.isOpened').removeClass('isOpened').find('.nav__list--inner').stop(true,true).slideUp(duration)
    }
  };
  function _clearNav() {
    var width = $(this).width();
    if (width > 1023 && !$('html').hasClass('ie9')) {
      $('.header__nav').hide();
      $('.hamburger').removeClass('isOpened');
    }
  };
  return {
    init: init
  };
})();

navAccordeon.init();
