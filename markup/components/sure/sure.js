/*************************************************
* Sure
*/

var sureModule = (function () {
  function init() {
    _addEventListeners();
  };

  function _addEventListeners() {
    var logo = $('.header__logo-link');
    var btnRed = $('.button--sure-red');

    $(logo).on('click', _showPopup);
    $(btnRed).on('click', _closePopup);
  };

  function _showPopup(e) {
    var popup = $('.sure'),
        duration = 300;
    if (popup.length) {
      e.preventDefault();
      $(popup).fadeIn(duration);
    }
  };

  function _closePopup() {
    var popup = $('.sure'),
        duration = 300
    $(popup).fadeOut(duration);
  }

  return {
    init: init
  }
})();

sureModule.init();
