var data = {
  bankAccountDetails: {
    title: 'Coordianate bancarie per il pagamento tramite bonifico',
    bankName: 'Banca Monte dei Paschi di Siena S.p.A.',
    accountDetails: {
      'IBAN:': 'IT 92 P 01030 62420 000000075922',
      'ABI:': '01030',
      'C/C N.': '000000075922',
      'BIC-SWIFT:': 'PASCITMXXX'
    }
  }
};
