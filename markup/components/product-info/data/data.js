var data = {
  productInfo: {
    title: 'Polo corporate',
    subtitle: 'Codice 0321456789',
    currency: '€',
    oldPrice: '38,50',
    price: '30,80',
    sale: '(prezzp scontato del 20%)',
    description: {
      title: 'Descrizione:',
      text: 'Polo 100% cotone. Sportiva e iconica. Un capo chiave dell’abbigliamento maschile, la polo manica corta in piquet. Personalizzata dall’elemento caratterizzante, lo Scudetto AC sul cuore.'
    },
    notes: {
      title: 'Note',
      notes: [
        'prodotto non è soggetto a sconti',
        'questo prodotto non viene spedito fisicamente con l’ordine, quindi non viene considerato per i costi'
      ]
    }
  }
};
