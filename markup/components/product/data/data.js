var data = {
  productHome: [
    {
      mod: 'disabled',
      src: 'hat-1.jpg',
      title: 'Cappellino Camouflage',
      price: '€ 12.00',
      hover: true,
      btnCount: 1,
      stickers: [
        'sale',
        'new',
        'top'
      ]
    },
    {
      mod: 'disabled',
      src: 'hat-2.jpg',
      title: 'Cappellino',
      price: '€ 9.90',
      hover: true,
      btnCount: 1,
      stickers: [
        'new'
      ]
    },
    {
      src: 'hat-3.jpg',
      title: 'Cappellino Di Paglia',
      price: '€ 2.00',
      hover: true,
      btnCount: 2
    },
    {
      src: 't-shirt.jpg',
      title: 'T-shirt',
      price: '€ 9.90',
      hover: true,
      btnCount: 2,
      stickers: ['top']
    }
  ]
};
