var data = {
  contactsFooter: {
    name: "Antonio Carraro Spa",
    address: "Via Caltana, 24-35011",
    city: "Campodarsego (Padova) Italia",
    linksTitle: "Contatti Istituzionali",
    www: [
      'www.antoniocarraro.it',
      'info@antoniocarraro.it'
    ]
  }
};