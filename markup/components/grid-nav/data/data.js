var data = {
  gridNav: [
    {
      title: "Corporate line",
      src: "corporate.jpg"
    },
    {
      title: "Fashion line",
      src: "fashion.jpg"
    },
    {
      title: "Kids line",
      src: "kids.jpg"
    },
    {
      title: "Da non perdere",
      src: "dont-miss.jpg"
    }
  ]
};