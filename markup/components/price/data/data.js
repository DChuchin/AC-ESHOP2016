var data = {
  popupPrice: {
    mod: 'row',
    oldPrice: '38,50',
    newPrice: '30,80',
    currency: '€',
    sale: '(prezzo scontato del 20%)'
  },
  productCardPrice: {
    mod: 'column',
    oldPrice: '38,50',
    newPrice: '30,80',
    currency: '€'
  },
  productCardPriceTotal: {
    oldPrice: '61,50',
    currency: '€'
  }
}
