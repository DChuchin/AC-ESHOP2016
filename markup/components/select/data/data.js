var data = {
  size: {
    title: 'Taglia',
    options: [
      '48',
      '50',
      '52',
      '54'
    ],
    mod: 'size'
  },
  color: {
    title: 'Colore',
    options: [
      'Rosso',
      'Verde',
      'Giallo',
      'Nero'
    ],
    mod: 'color'
  },
  day: {
    mod: 'checkout',
    options: [
      'Giorno *',
      1,
      2,
      3
    ]
  },
  month: {
    mod: 'checkout',
    options: [
      'Mese *',
      'option 1',
      'option 2',
      'option 3'
    ]
  },
  year: {
    mod: 'checkout',
    options: [
      'Anno *',
      'option 1',
      'option 2',
      'option 3'
    ]
  }
};
