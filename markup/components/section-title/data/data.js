var data = {
  home: {
    title: "in evidenza",
    size: 'h2'
  },
  product: {
    big: {
      title: 'Fashion line',
      size: 'h2',
      link: 'Ritorna ai prodotti fashion line'
    },
    small: {
      title: 'potrebbero interessarti anche',
      size: 'h3'
    }
  },
  cart: {
    big: {
      title: 'Carrello',
      size: 'h2',
      link: 'Ritorna in homepage'
    },
    small: {
      title: "Riepilogo dell'ordine",
      size: 'h3'
    }
  },
  checkout: {
    big: {
      title: 'Concludi l’ordine',
      size: 'h2',
      link: 'Ritorna in homepage'
    },
    small: {
      title: 'Riepilogo dell’ordine',
      size: 'h3'
    }
  },
  orderCompleted: {
    title: 'Grazie',
    size: 'h2'
  },
  faq: {
    title: 'Info & FAQ',
    size: 'h2'
  },
  orderList: {
    title: 'Area Clienti',
    size: 'h2'
  },
  loginRegister: {
    title: 'Service',
    size: 'h2'
  }
};
