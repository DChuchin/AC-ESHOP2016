
/*************************************************
*  Responsive sliding sidebar
*/

var slideSidebar = (function() {
  function init() {
    _setUpListeners();
  };
  function _setUpListeners() {
    $('.slide-arrow').on('click', _toggleSlide);
  };
  function _toggleSlide(e) {
    e.preventDefault();
    var $this = $(this),
        sidebar = $this.closest('.sidebar');
    if (sidebar.hasClass('open')) {
      sidebar.removeClass('open');
    } else {
      sidebar.addClass('open');
    }
  }
  return {
    init: init
  };
})();

slideSidebar.init();
