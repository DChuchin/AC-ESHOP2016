
/*************************************************
*  search input in header
*/
var searchModule = (function () {

  function startSearch(value) {
    
  };

  function _focus(elem) {
    let input = elem.siblings('input');
    let value = input.val();
    if (input.hasClass('focus')) {
      if (value) {
        startSearch(value);
        input.val('');
      }
      input.removeClass('focus');
    } else {
      input.addClass('focus');
    }
  };

  function _setUpListeners() {
    let lens = $('.search__submit');
    if (lens.length) {
      lens.on('click', function (e) {
        e.preventDefault();
        _focus(lens);
      });
    }
  };

  function init() {
    _setUpListeners();
  };

  return {
    init: init
  };
})();

searchModule.init();
