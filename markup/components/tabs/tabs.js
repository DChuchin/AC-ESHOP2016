/*************************************************
* Tabs
*/
var tabsModule = (function() {
  function init() {
    _addEventListeners();
  };

  function _addEventListeners() {
    var tablist = $('.tabs__list');
    $(tablist).on('click', '.tabs__link', _showTabContent);
  };

  function _showTabContent(e) {
    e.preventDefault();
    var link = e.target,
        item = $(link).closest('.tabs__item'),
        siblingsLinks = $(item).siblings().find('.tabs__link'),
        id = $(link).data('id'),
        content = $('.tabs__content-item'),
        top = $('.tabs__content').offset().top;

    if (!$(link).hasClass('.active')) {
      var id = $(link).data('id');
      $(siblingsLinks).removeClass('active');
      $(link).addClass('active');
      $(content).each(function(i,item) {
        if ($(item).data('id') == id) {
          $(item).addClass('active');
        } else {
          $(item).removeClass('active');
        }
      })
    }
    _scrollToContent(top);
  };
  
  function _scrollToContent(top) {
    $('html, body').animate({scrollTop: top - 50}, 300);
  };

  return {
    init: init
  }
})();

tabsModule.init();
