var data = {
  tabsFaq: {
    links: [
      'Guida all’acquisto',
      'Pagamenti',
      'Spedizioni',
      'Condizioni',
      "Stato dell'ordine",
      "Resi",
      "Domande frequenti"
    ],
    content: 'buying guide'
  },
  tabsOrderList: {
    links: [
      'I miei ordini',
      'Modivica i dati di registrazione',
      'Modifica i dati di fatturazione',
      'Modivica i dati di spedizione',
      "Elimina il tuo profilo"
    ],
    content: 'order list'
  },
  tabsUserData: {
    links: [
      'I miei ordini',
      'Modivica i dati di registrazione',
      'Modifica i dati di fatturazione',
      'Modivica i dati di spedizione',
      "Elimina il tuo profilo"
    ],
    content: 'user data'
  }
};
