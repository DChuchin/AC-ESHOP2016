/*************************************************
* Product slider
*/

$('.product-slider--small').slick({
  dots: false,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  nextArrow: '<button type="button" class="product-slider__btn product-slider__btn--next">Next</button>',
  prevArrow: '<button type="button" class="product-slider__btn product-slider__btn--prev">Prev</button>',
  responsive: [
    {
      breakpoint: 720,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
$('.product-slider--big').on('init', function() {
  var $this = $(this);
  $(document).ready(function() {
    
  })
})
var options = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow: '<button type="button" class="product-slider__btn product-slider__btn--next">Next</button>',
  prevArrow: '<button type="button" class="product-slider__btn product-slider__btn--prev">Prev</button>'
} 
$('.product-slider--big').slick(options);

var changeSliderModule = (function(options) {
  function init() {
    _setUpListeners();
  };
  function _setUpListeners() {
    var select = $('.select__label--color').find('.select__select');
    
    select.on('change', _changeSlide);
  };
  function _changeSlide() {
    var $this = $(this),
        color = $this.val().toLowerCase();
    $('.product-slider--big').each(function(i, el){
      $(el).data('color');
      if ($(el).data('color').toLowerCase() == color) {
        $(el).removeClass('isHidden');
      } else {
        $(el).addClass('isHidden');
      }
    });
  };
  return {
    init: init
  };
})(options);
$(document).ready(function() {
  changeSliderModule.init();
  
})
