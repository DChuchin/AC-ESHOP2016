var data = {
  productSliderBig: {
    mod: "big",
    slides: [
      {
        mod: 'slide-big',
        src: 't-shirt-big.jpg',
        hover: false,
        stickers: [
          'sale'
        ]
      },
      {
        mod: 'slide-big',
        src: 't-shirt-big.jpg',
        hover: false,
        stickers: [
          'top',
          'sale',
          'new'
        ]
      },
      {
        mod: 'slide-big',
        src: 't-shirt-big.jpg',
        hover: false,
        stickers: [
          'top',
          'new'
        ]
      }
    ]
  },
  productSliderSmall: {
    mod:'small',
    slides: [
      {
        mod: 'slide-small',
        src: 'hat-1.jpg',
        title: 'Cappellino Camouflage',
        price: '€ 12.00',
        hover: false
      },
      {
        mod: 'slide-small',
        src: 'hat-2.jpg',
        title: 'Cappellino',
        price: '€ 9.90',
        hover: false,
      },
      {
        mod: 'slide-small',
        src: 'hat-3.jpg',
        title: 'Cappellino Di Paglia',
        price: '€ 2.00',
        hover: false,
      },
      {
        mod: 'slide-small',
        src: 't-shirt.jpg',
        title: 'T-shirt',
        price: '€ 9.90',
        hover: false
      }
    ]
  }
};
