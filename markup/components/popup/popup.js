
/*************************************************
* Product popup
*/

var popupModule = (function() {
  function _setUpListeners() {
    // listening of events
    $('.button--sidebar').on('click', openPopup);
    $('.popup').on('click', closePopup);
  };

  function init() {
    _setUpListeners();
  };

  function closePopup(e) {
    e.preventDefault();
    var target = e.target;
    if ($(target).hasClass('popup')||($(target).hasClass('popup__close-btn'))) {
      $(this).fadeOut(300);
      $('body').removeClass('fixed');
    }
  };

  function openPopup(e) {
    $('.popup').fadeIn(300);
    $('body').addClass('fixed');
  };

  return {
    init: init
  }
})();

popupModule.init();
