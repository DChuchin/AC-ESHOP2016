
var data = {
  share: {
    title: 'condivi',
    mod: 'share',
    socials: {
      fb: {
        img: '$fb-footer',
        hover: '$fb-footer-hover',
        title: 'facebook',
        faClass: 'fa-facebook-square',
        mod: 'fb'
      },
      tw: {
        img: '$tw-footer',
        hover: '$tw-footer-hover',
        title: 'twitter',
        faClass: 'fa-twitter-square',
        mod: 'tw'
      },
      ggl: {
        img: '$ggl-footer',
        hover: '$ggl-footer-hover',
        title: 'google plus',
        faClass: 'fa-google-plus-square',
        mod: 'ggl'
      }
    }
  },
  footer: {
    mod: 'footer',
    socials: {
      fb: {
        img: '$fb-footer',
        hover: '$fb-footer-hover',
        title: 'facebook',
        faClass: 'fa-facebook-square',
        mod: 'fb'
      },
      tw: {
        img: '$tw-footer',
        hover: '$tw-footer-hover',
        title: 'twitter',
        faClass: 'fa-twitter-square',
        mod: 'tw'
      },
      ggl: {
        img: '$ggl-footer',
        hover: '$ggl-footer-hover',
        title: 'google plus',
        faClass: 'fa-google-plus-square',
        mod: 'ggl'
      },
      yt: {
        img: '$yt-footer',
        hover: '$yt-footer-hover',
        title: 'youtube',
        faClass: 'fa-youtube-square',
        mod: 'yt'
      }
    }
  }
};
