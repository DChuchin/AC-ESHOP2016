var data = {
  taxPrice: {
    text: 'VIA',
    num: '20,92',
    currency: '€'
  },
  shipPrice: {
    text: 'Spese di spedizone',
    num: '12,00',
    currency: '€'
  },
  totalPrice: {
    mod: 'total-big',
    text: 'Totale',
    num: '128,02',
    currency: '€'
  },
  totalPriceSmall: {
    mod: 'total-small',
    text: 'Sub totale',
    num: '98,60',
    currency: '€'
  },
  productCheck: {
    mod: 'product-check',
    text: 'totale',
    num: '98,60',
    currency: '€'
  },
  promoSale: {
    text: '',
    mod: 'promo',
    num: '95,10',
    currency: '- €'
  },
  promoCode: {
    text: 'Codice promo',
    boldText: ' PROMO 16',
    mod: 'checkout-promo',
    num: '95,10',
    currency: '- €'
  }
};
