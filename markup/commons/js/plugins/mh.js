'use strict';
$(document).ready(function(){
  $('.sidebar, .page__content').matchHeight();
});
$(window).resize(function() {
  setTimeout(function() {
    $.fn.matchHeight._update();
  },0);
});
